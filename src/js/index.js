(function () {
	// Javascript to enable link to tab
	function showTab() {
		var url = document.location.toString();

  		if (sessionStorage.getItem('state')) {
  			window.location.href = window.location.href + '#' + sessionStorage.getItem('state');
	    	$('.nav-tabs a[data-target="#' + sessionStorage.getItem('state') + '"]').tab('show');
	    	setTimeout(function () {
				$(document).scrollTop(0, 0);
	    	}, 0);
	    	sessionStorage.setItem('state', '');
		} else if (url.match('#')) {
			$('.nav-tabs a[data-target="#' + url.split('#')[1] + '"]').tab('show');
		} else {
			$('.nav-tabs a[data-target="#about"]').tab('show');
		}
	}
	
	showTab();

	// Change hash for page-reload
	$('.nav-tabs a').on('shown.bs.tab', function(e) {
		var target = $(e.target).data('target');
		var url = document.location.toString();

		$(window).width() < 960? $('nav').addClass('nav-hidden-mobile'): null;
		
    	url.match('#')? url = url.split("#")[0]: null;
    	
    	history.pushState(null, null, url + target);
	});

	window.onpopstate = function(event) {
		showTab();
	};
})();


$('.nav-toggle').on('click', function () {
	var nav = $('nav');
	nav.hasClass('nav-hidden-mobile')? nav.removeClass('nav-hidden-mobile'): nav.addClass('nav-hidden-mobile');
});

$(document).ready(function () {
	$('.lang-switch').on('click', function (e) {
		e.preventDefault();
		var href = $(e.target).data('href');
		sessionStorage.setItem('state', document.location.toString().split('#')[1]);
		setTimeout(function () {
			window.location.href = href;
		}, 0);
	});
});